TARGET = output

CC = arm-none-eabi-gcc
LD = arm-none-eabi-ld
AR = arm-none-eabi-ar
AS = arm-none-eabi-as
CP = arm-none-eabi-objcopy
OD = arm-none-eabi-objdump
SE = arm-none-eabi-size
SF = st-flash

CFLAGS  = -g -c -Os -Wall -nostdinc -ffreestanding -fno-pie
CFLAGS += -mcpu=cortex-m3 -march=armv7-m -mlittle-endian -mthumb -mno-thumb-interwork
CFLAGS += -I. -Ilib -Iusb -Istm32f103xb
#CFLAGS += -DSTDLIB_HEAP_SIZE=1024

AFLAGS = -g

LDFLAGS = -T stm32f103xb/link.ld -nostartfiles -nostdlib -nodefaultlibs --gc-sections

OBJS  = stm32f103xb/ivt.o
OBJS += lib/startup.o lib/io_itm.o lib/io.o lib/stdlib.o lib/string.o
OBJS += start.o usb/usb.o

.PHONY: $(TARGET)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(LD) $(LDFLAGS) -o $@.elf $(OBJS) -L. -lgcc
	$(CP) -O binary $@.elf $@

clean:
	rm -f *.o $(TARGET).elf $(TARGET) $(OBJS)

flash: $(TARGET)
	$(SF) --reset write $(TARGET) 0x8000000
