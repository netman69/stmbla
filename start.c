#include <stdint.h>
#include <string.h>
#include "usb.h"
#include "device.h"
#include "io_itm.h"

const usb_device_t desc_dev = {
	.bLength = sizeof(usb_device_t),
	.bDescriptorType = 0x01,
	.bcdUSB = 0x0002, // TODO Is this correct?
	.bDeviceClass = 0x02,
	.bDeviceSubClass = 0,
	.bDeviceProtocol = 0,
	.bMaxPacketSize = 64,
	.idVendor = 0x25AE,
	.idProduct = 0x24AB,
	.bcdDevice = 0x1,
	.iManufacturer = 1,
	.iProduct = 2,
	.iSerialNumber = 3,
	.bNumConfigurations = 1
};

const struct {
	usb_configuration_t config;
	usb_interface_t interface_comm;
	usb_endpoint_t endpoint_comm;
	struct {
		uint8_t bLength;
		uint8_t bDescriptorType;
		uint8_t bDescriptorSubType;
		uint16_t bcdCDC;
	} __attribute__((packed)) fun_hdr;
	struct {
		uint8_t bLength;
		uint8_t bDescriptorType;
		uint8_t bDescriptorSubType;
		uint8_t bmCapabilities;
		uint8_t bDataInterface;
	} __attribute__((packed)) fun_cmgmt;
	struct {
		uint8_t bLength;
		uint8_t bDescriptorType;
		uint8_t bDescriptorSubType;
		uint8_t bmCapabilities;
	} __attribute__((packed)) fun_acm;
	struct {
		uint8_t bLength;
		uint8_t bDescriptorType;
		uint8_t bDescriptorSubType;
		uint8_t bMasterInterface;
		uint8_t bSlaveInterface0;
	} __attribute__((packed)) fun_union;
	usb_interface_t interface_data;
	usb_endpoint_t endpoint_data_out;
	usb_endpoint_t endpoint_data_in;
} __attribute__((packed)) desc_conf = {
	.config = {
		.bLength = sizeof(usb_configuration_t),
		.bDescriptorType = 0x02,
		.wTotalLength = sizeof(desc_conf),
		.bNumInterfaces = 2,
		.bConfigurationValue = 1,
		.iConfiguration = 0,
		.bmAttributes = 0xA0, /* Bus powered, remote wakeup enabled. */
		.bMaxPower = 0x32 /* 100mA */
	},
	.interface_comm = {
		.bLength = sizeof(usb_interface_t),
		.bDescriptorType = 0x04,
		.bInterfaceNumber = 0,
		.bAlternateSetting = 0,
		.bNumEndpoints = 1,
		.bInterfaceClass = 0x02, /* Communication Interface Class (the only option for CDC). */
		.bInterfaceSubClass = 0x02, /* Abstract Control Model (CDC-ACM). */
		.bInterfaceProtocol = 0x01, /* AT commands. */
		.iInterface = 0
	},
	.endpoint_comm = {
		.bLength = sizeof(usb_endpoint_t),
		.bDescriptorType = 0x05,
		.bEndpointAddress = 0x83,
		.bmAttributes = 0x03, /* Interrupt Transport. */
		.wMaxPacketSize = 16,
		.bInterval = 255
	},
	.fun_hdr = {
		.bLength = sizeof(desc_conf.fun_hdr),
		.bDescriptorType = 0x24,
		.bDescriptorSubType = 0x00,
		.bcdCDC = 0x0110
	},
	.fun_cmgmt = {
		.bLength = sizeof(desc_conf.fun_cmgmt),
		.bDescriptorType = 0x24,
		.bDescriptorSubType = 0x01,
		.bmCapabilities = 0,
		.bDataInterface = 1
	},
	.fun_acm = {
		.bLength = sizeof(desc_conf.fun_acm),
		.bDescriptorType = 0x24,
		.bDescriptorSubType = 0x02,
		.bmCapabilities = 0
	},
	.fun_union = {
		.bLength = sizeof(desc_conf.fun_union),
		.bDescriptorType = 0x24,
		.bDescriptorSubType = 0x06,
		.bMasterInterface = 0,
		.bSlaveInterface0 = 1
	},
	.interface_data = {
		.bLength = sizeof(usb_interface_t),
		.bDescriptorType = 0x04,
		.bInterfaceNumber = 1,
		.bAlternateSetting = 0,
		.bNumEndpoints = 2,
		.bInterfaceClass = 0x0A, /* Data Interface Class. */
		.bInterfaceSubClass = 0,
		.bInterfaceProtocol = 0,
		.iInterface = 0,
	},
	.endpoint_data_out = {
		.bLength = sizeof(usb_endpoint_t),
		.bDescriptorType = 0x05,
		.bEndpointAddress = 0x01,
		.bmAttributes = 0x02, /* Bulk Transport. */
		.wMaxPacketSize = 64,
		.bInterval = 1
	},
	.endpoint_data_in = {
		.bLength = sizeof(usb_endpoint_t),
		.bDescriptorType = 0x05,
		.bEndpointAddress = 0x82,
		.bmAttributes = 0x02, /* Bulk Transport. */
		.wMaxPacketSize = 64,
		.bInterval = 1
	}
};

char desc_str_langid[] = { 0x04, 0x03, 0x09, 0x04 };
char desc_str_vendor[] = { 34, 0x03, 'M', 0, 'A', 0, 'T', 0, ' ', 0, 'I', 'n', 0, 'c', 0, 'o', 0, 'r', 0, 'p', 0, 'o', 0, 'r', 0, 'a', 0, 't', 0, 'e', 0, 'd', 0  };
char desc_str_product[] = { 24, 0x03, 'S', 0, 'T', 0, 'M', 0, '3', 0, '2', 0, ' ', 0, 'T', 0, 'h', 0, 'i', 0, 'n', 0, 'g', 0  };
char desc_str_serial[] = { 10, 0x03, 'V', 0, '2', 0, '.', 0, '0', 0  };

/* USB String Descriptors (optional). */
const char *desc_strings[] = { // TODO this does not work
	desc_str_langid,
	desc_str_vendor,
	desc_str_product,
	desc_str_serial,
	NULL
};

void usbh_reset(void) { // TODO The usb library could handle this I think. But should it?
	/* Initialize endpoint 0x01. */
	usb_init_ep(1, MUSB_EP_BULK, 64, 0);
	MUSB_EP_RXTX_STAT_SET(1, MUSB_EP_RX_VALID | MUSB_EP_TX_DIS);
	/* Initialize endpoint 0x82. */
	usb_init_ep(2, MUSB_EP_BULK/* | MUSB_EP_KIND*/, 0 /*64*/, 64);
	MUSB_EP_RXTX_STAT_SET(2, MUSB_EP_RX_DIS | MUSB_EP_TX_NAK);
	/* Initialize endpoint 0x83. */
	usb_init_ep(3, MUSB_EP_INTERRUPT, 0, 16);
	MUSB_EP_RXTX_STAT_SET(3, MUSB_EP_RX_DIS | MUSB_EP_TX_NAK);
}

int lstate = 0; // TODO wat?

bool usbh_setup(usb_setup_t *s, void *data) {
	if (usb_state != MUSB_STATE_CONFIGURED)
		return false;
	if (s->bmRequestType == 0x02 && s->bRequest == 0x01) { // CLEAR_FEATURE
		//usb_ctrl_write((void *) rep, sizeof(rep));
		//MUSB_EP_TX_STAT_SET(s->wIndex, MUSB_EP_TX_STALL); // TODO does this make a sense?
		usb_ctrl_write(NULL, 0);
		return true;
	}
	if (s->bmRequestType == 0x21 && s->bRequest == 0x20) { // USB_DEVICE_CDC_REQUEST_SET_LINE_CODING
		mitmprintf("set line coding rate=%d stopbits=%d paritytype=%d databits=%d\n", *(uint32_t *) data, *(uint8_t *) (data + 4), *(uint8_t *) (data + 5), *(uint8_t *) (data + 6));
		usb_ctrl_write(NULL, 0);
		return true;
	}
	if (s->bmRequestType == 0x21 && s->bRequest == 0x22) { // USB_DEVICE_CDC_REQUEST_SET_CONTROL_LINE_STATE
		lstate = s->wValue;
		//MUSB_EP_RX_STAT_SET(1, MUSB_EP_RX_VALID);
		//MUSB_EP(1) = MUSB_EP_MASKED(1) | MUSB_EP_DTOG_RX;
		/*usb_ctrl_write((void *) rep, sizeof(rep));*/
		
		/*volatile struct {
			uint8_t bmRequestType;
			uint8_t bNotificationType;
			uint16_t wValue;
			uint16_t wIndex;
			uint16_t wLength;
			uint8_t a, b;
		} volatile rep = {
			0xA1, 0x20, 0, 0, 2, // Beforelast one is Interface #
			3, 0
		};*/
		//usb_write(3, (void *) &rep, 16);
		
		usb_ctrl_write(NULL, 0);
		return true;
	}
	// Windows 7 gives: Setup0 request. A1 21 0 0 7
	mitmprintf("Setup0 request. %X %X %X %X %X\n", s->bmRequestType, s->bRequest, s->wValue, s->wIndex, s->wLength);
	return false;
}

int suspend = 0;
volatile int txready = 0;

void usbh_suspend(void) {
	suspend = 1;
}

void usb_suspend(void) { // TODO put back in usbh_suspend
	/* Turn off LED and port C. */
	GPIOC->ODR |= (1 << 13);
	RCC->APB2ENR &= ~RCC_APB2ENR_IOPCEN;
	/* Enter STOP mode after this interrupt finishes. */
	PWR->CR |= PWR_CR_CWUF | PWR_CR_LPDS;
	PWR->CR &= ~PWR_CR_PDDS;
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk | SCB_SCR_SLEEPONEXIT_Msk;
}

int count = 0; // TODO remove

void usbh_ep_in(uint8_t ep) {
	//mitmprintf("* usb EP in %d\n", ep);
	if (ep == 2)
		txready = 1;
}

void usbh_ep_out(uint8_t ep) {
	//mitmprintf("+ usb EP out %d\n", ep);
	if (ep == 1) {
		char str[65];
		int len = MUSB_BT_COUNT_RX(1) & 0x3FF;

		usb_pma_read(MUSB_BT_ADDR_RX(1), (void *) str, len);
		str[len] = 0;

		//mitmprintf("recv: %s\n", str); // TODO Remove.

		if (lstate & 1) {
			if (len == 1 && str[0] == 0x0D) {
				char nl[2] = { 0x0D, 0x0A };
				MUSB_BT_COUNT_TX(2) = 2;
				usb_pma_write(MUSB_BT_ADDR_TX(2), (void *) nl, 2);
				MUSB_EP_TX_STAT_SET(2, MUSB_EP_TX_VALID);
			} else { // Echo it.
				MUSB_BT_COUNT_TX(2) = len;
				usb_pma_write(MUSB_BT_ADDR_TX(2), (void *) str, len);
				MUSB_EP_TX_STAT_SET(2, MUSB_EP_TX_VALID);
			}
		}
		MUSB_EP_RXTX_STAT_SET(ep, MUSB_EP_RX_VALID | MUSB_EP_TX_STALL);
	}
	//mitmprintf("- usb EP out %d end\n", ep);
}

void clk_init(void) {
	/* Clock configuration for 48Mhz. */
//	FLASH->ACR = FLASH_ACR_PRFTBE | 1;
//	RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW | RCC_CFGR_HPRE | RCC_CFGR_PPRE1 | RCC_CFGR_PPRE2 | RCC_CFGR_PLLXTPRE_HSE_DIV2 | RCC_CFGR_PLLMULL)) |
//		RCC_CFGR_SW_PLL | RCC_CFGR_HPRE_DIV1 | RCC_CFGR_PPRE1_DIV2 | RCC_CFGR_PPRE2_DIV1 | RCC_CFGR_PLLSRC | RCC_CFGR_PLLMULL6 | RCC_CFGR_USBPRE;
	/* Clock configuration for 72Mhz. */
	FLASH->ACR = FLASH_ACR_PRFTBE | 2; /* When using 2 wait states do not clear FLITFEN bit in RCC (resets to 1) see stm32f10xx8 errata for why. */
	RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW | RCC_CFGR_HPRE | RCC_CFGR_PPRE1 | RCC_CFGR_PPRE2 | RCC_CFGR_PLLXTPRE_HSE_DIV2 | RCC_CFGR_PLLMULL | RCC_CFGR_USBPRE)) |
		RCC_CFGR_SW_PLL | RCC_CFGR_HPRE_DIV1 | RCC_CFGR_PPRE1_DIV2 | RCC_CFGR_PPRE2_DIV1 | RCC_CFGR_PLLSRC | RCC_CFGR_PLLMULL9;
	RCC->CR |= RCC_CR_HSEON | RCC_CR_PLLON;
	/* Wait until clock ready. */
	while (!((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL));
}

void gpio_init(void) {
	/* Enable Port B and C clock and AFIO clock (needed for interrupt). */
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_AFIOEN;
	/* Set PB8 and PB9 as floating input. */
	GPIOB->CRH = (GPIOB->CRH & ~(0x0F << ((8 - 8) * 4))) | (0x04 << ((8 - 8) * 4));
	GPIOB->CRH = (GPIOB->CRH & ~(0x0F << ((9 - 8) * 4))) | (0x04 << ((9 - 8) * 4));
	/* Set interrupt mask. */
	EXTI->IMR |= (1 << 8) | (1 << 9);
	EXTI->RTSR |= (1 << 8) | (1 << 9); /* Enable rising trigger. */
	EXTI->FTSR |= (1 << 8) | (1 << 9); /* Enable falling trigger. */
	/* Set where from IRQ should come, and enable IRQ. */
	AFIO->EXTICR[2] &= ~(AFIO_EXTICR3_EXTI8 | AFIO_EXTICR3_EXTI9);
	AFIO->EXTICR[2] |= AFIO_EXTICR3_EXTI8_PB | AFIO_EXTICR3_EXTI9_PB;
	//NVIC_EnableIRQ(EXTI9_5_IRQn);
	NVIC_SetPriority(EXTI9_5_IRQn, 0);
	/* Configure GPIO pin: PC13 (onboard LED). */
	GPIOC->ODR |= (1 << 13); /* Turn it off (LED is wired between pin and VDD). */
	GPIOC->CRH = (GPIOC->CRH & ~(0x0F << ((13 - 8) * 4))) | (0x06 << ((13 - 8) * 4));/* 2Mhz open drain output. */
}

static inline __attribute__((always_inline)) void __DISABLE_IRQ(void) {
	__asm volatile("cpsid i");
}

/*enable all interrupts*/
static inline __attribute__((always_inline)) void __ENABLE_IRQ(void) {
	__asm volatile("cpsie i");
}

void USB_LP_CAN1_RX0_IRQHandler(void) {
	NVIC_DisableIRQ(USB_LP_CAN1_RX0_IRQn);
	__DISABLE_IRQ();
	usb_int();
	__ENABLE_IRQ();
	NVIC_EnableIRQ(USB_LP_CAN1_RX0_IRQn);
}

void USBWakeUp_IRQHandler(void) {
	/* Resume */
	clk_init();
	MUSB_CNTR &= ~(MUSB_CNTR_FSUSP | MUSB_CNTR_LP_MODE);
	SCB->SCR &= ~SCB_SCR_SLEEPONEXIT_Msk;
	/* Turn port C back on. */
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	/* Done. */
	EXTI->PR |= EXTI_PR_PR18;
}

void EXTI9_5_IRQHandler(void) {
	/* Resume the system clock and request remote wakeup. */
	if (usb_state & MUSB_STATE_SUSPENDED_FLAG) {
		clk_init();
		usb_rwakeup();
	}
	SCB->SCR &= ~SCB_SCR_SLEEPONEXIT_Msk;
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN; /* Also make sure port C is re-enabled. */
	///* Check on the inputs. */
	//input_hist = (input_hist <<2) & 0x3F;
	//input_hist |= ((GPIOB->IDR & (1 << 8)) >> 8) | ((GPIOB->IDR & (1 << 9)) >> 8);
	//if ((input_hist == 0x1E || input_hist == 0x2D) && usb_state == MUSB_STATE_CONFIGURED) {
		//if (input_hist == 0x1E) {
			//if (usb_hid_report[3] < 127)
				//++usb_hid_report[3];
			//if (--encoder_pos < 0)
				//encoder_pos = 99;
		//} else {
			//if (usb_hid_report[3] > -127)
				//--usb_hid_report[3];
			//if (++encoder_pos > 99)
				//encoder_pos = 0;
		//}
		//usb_hid_report_send();
		//mitmprintf("pos: %2d\n", encoder_pos);
	//}

	/* Exit interrupt. */
	EXTI->PR |= EXTI_PR_PR8 | EXTI_PR_PR9;
}

void start(void) {
	clk_init();
	gpio_init();

	/* Enable USBWakeUp IRQ, make sure to do this before USB can suspend. */
	NVIC_EnableIRQ(USBWakeUp_IRQn);
	NVIC_SetPriority(USBWakeUp_IRQn, 0);
	EXTI->IMR |= (1 << 18); /* Unmask the interrupt. */
	EXTI->RTSR |= (1 << 18); /* Select rising trigger. */

	/* Enable USB peripheral. */
	RCC->APB1ENR |= RCC_APB1ENR_USBEN | RCC_APB1ENR_PWREN;
	NVIC_EnableIRQ(USB_LP_CAN1_RX0_IRQn);
	NVIC_SetPriority(USB_LP_CAN1_RX0_IRQn, 1);

	mitmprintf("USB init start! %X %X %X\n", NVIC_GetActive(USBWakeUp_IRQn), NVIC_GetPendingIRQ(USBWakeUp_IRQn), (1 << 18));
	usb_init(&desc_dev, (usb_configuration_t *) &desc_conf, desc_strings);

	mitmprintf("Loop reached! Num test: 0x%X 0x%X\n", 0x12345678, 0xABCDEF01);

		static char *str = "testaaaaaaaaaaaasdadfadfsdfadfsafdsafasdfaaaaaaaaaafsdafdafdsa\r\n";
		int len = strlen(str);
	/* Infinite loop. */
	uint32_t a = 0;
	while (1) {
		++a;
		if (a % 100000 > 50000) {
			GPIOC->ODR |= (1 << 13);
		} else GPIOC->ODR &= ~(1 << 13);

		if (suspend) {
			usb_suspend();
			suspend = 0;
		}
		//mitmprintf("testing\n");

		while (!txready);
		txready = 0;
		//if ((MUSB_EP(2) & (MUSB_EP_DTOG_RX | MUSB_EP_DTOG_TX)) == MUSB_EP_DTOG_TX) {
			// TODO this is just here to test troughput, i should try where it can get with dbl buffering
			/*MUSB_BT_COUNT_TX(2) = len;
			usb_pma_write(MUSB_BT_ADDR_TX(2), (void *) str, len);
			MUSB_EP_TX_STAT_SET(2, MUSB_EP_TX_VALID);*/
		/*} else {
			MUSB_BT_COUNT_RX(2) = len;
			usb_pma_write(MUSB_BT_ADDR_RX(2), (void *) str, len);
			MUSB_EP_TX_STAT_SET(2, MUSB_EP_TX_VALID);
		}*/

		/*char *str = "Hello!";
		usb_btable_t *bt = &MUSB_BT(2);
		usb_pma_write(bt->ADDR_TX, (void *) str, sizeof(str));
		bt->COUNT_TX = sizeof(str);
		MUSB_EP_TX_STAT_SET(1, MUSB_EP_TX_VALID);*/
	}
}
