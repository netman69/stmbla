/* MAT string.h for STM32.
 * 
 * Copyright (C) 2018 Mattis Michel.
 */
#ifndef __STRING_H__
#define __STRING_H__

#include <stddef.h> /* size_t */

extern size_t strlen(char *str);
extern void *memset(void *dest, int c, size_t n);
extern void *memcpy(void *dest, const void *src, size_t n);

#endif /* __STRING_H__ */
