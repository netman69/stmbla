/* MAT stdlib.c for STM32.
 * 
 * Copyright (C) 2018 Mattis Michel.
 */
#include <stdlib.h>
#include <stdbool.h> /* bool */
#include <string.h> /* memcpy() */
#include <stddef.h> /* NULL, size_t */

/* To use this the linker must either define _heap and _heap_size symbols,
 *   or STDLIB_HEAP_SIZE must be defined telling what size should the heap be.
 *   This size must be given in multiples of 4 bytes (even for _heap_size !).
 *   Using the linker makes so the heap won't be zero-filled due to being in BSS.
 *
 * The heap will be layed out as follows:
 *   - For every separate allocatable or allocated block there will be a prefix.
 *   - That prefix is 32 bits, 31 of which are the size.
 *   - The most significant bit is a flag indicating whether the block is used.
 *   - There shall be no 2 consecutive free blocks, free() and realloc() will concatenate them.
 *   - Everything is aligned to 4-byte boundaries.
 */

#ifdef STDLIB_HEAP_SIZE
	uint32_t _heap[STDLIB_HEAP_SIZE];
#else
	extern uint32_t _heap[];
	extern volatile void _heap_size;
	#define STDLIB_HEAP_SIZE ((uint32_t) &_heap_size)
#endif

bool initialized = false;

#define HEAP_USEDBIT (1 << 31)
#define HEAP_MASK    (~HEAP_USEDBIT)

void stdlib_init(void) {
	_heap[0] = STDLIB_HEAP_SIZE - 1;
	initialized = true;
}

void *malloc(size_t size) {
	uint32_t o = 0, sz = size / 4 + (((size % 4) > 0) ? 4 : 0);
	if (!initialized)
		stdlib_init();
	while (o < STDLIB_HEAP_SIZE) {
		if ((_heap[o] & HEAP_USEDBIT) == 0 && sz <= _heap[o]) {
			if (sz < _heap[o]) /* If only using partial block, split it. */
				_heap[o + sz + 1] = _heap[o] - sz - 1;
			_heap[o] = sz | HEAP_USEDBIT;
			return _heap + o + 1;
		}
		o += (_heap[o] & HEAP_MASK) + 1;
	}
	return NULL;
}

void *realloc(void *ptr, size_t size) {
	uint32_t o = 0, next = 0, sz = size / 4 + (((size % 4) > 0) ? 4 : 0);
	if (!initialized)
		stdlib_init();
	/* NULL pointer means we should simply malloc(). */
	if (ptr == NULL)
		return malloc(size);
	/* Find index in heap. */
	while (o < STDLIB_HEAP_SIZE) {
		if (_heap + o + 1 == ptr)
			break;
		o += (_heap[o] & HEAP_MASK) + 1;
	}
	if (o >= STDLIB_HEAP_SIZE)
		return NULL; /* Invalid pointer. */
	next = o + (_heap[o] & HEAP_MASK) + 1;
	/* Check if we can extend this block. */
	if (next < STDLIB_HEAP_SIZE && (_heap[next] & HEAP_USEDBIT) == 0 && sz <= _heap[o] + 1 + _heap[next]) {
		if (sz < _heap[o] + 1 + _heap[next]) /* If only using partial block, split it. */
			_heap[o + sz + 1] = (_heap[o] & HEAP_MASK) - sz - 1;
		_heap[o] = sz | HEAP_USEDBIT;
		return ptr;
	}
	/* In case we can't extend the block, try making a new one. */
	{
		void *ret = malloc(size);
		if (ret == NULL)
			return NULL;
		memcpy(ret, ptr, size);
		free(ptr);
		return ret;
	}
}

void free(void *ptr) {
	uint32_t o = 0, prev = 0, next = 0;
	/* Find index in heap. */
	while (o < STDLIB_HEAP_SIZE) {
		if (_heap + o + 1 == ptr)
			break;
		prev = o;
		o += (_heap[o] & HEAP_MASK) + 1;
	}
	if (o >= STDLIB_HEAP_SIZE)
		return; /* Invalid pointer. */
	next = o + (_heap[o] & HEAP_MASK) + 1;
	/* Mark it as free. */
	_heap[o] &= HEAP_MASK;
	/* Merge adjacent free blocks */
	if (next < STDLIB_HEAP_SIZE && (_heap[next] & HEAP_USEDBIT) == 0)
		_heap[o] += 1 + _heap[next];
	if (prev != o && (_heap[prev] & HEAP_USEDBIT) == 0)
		_heap[prev] += 1 + _heap[o];
}
