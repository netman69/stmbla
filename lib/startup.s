.syntax unified
.cpu cortex-m3
.fpu softvfp
.thumb

.global _start
.extern start
.extern _sidata
.extern _sdata
.extern _edata
.extern _sbss
.extern _ebss

.section .text._start, "ax", %progbits
.type _start, %function
_start:
	/* Zero fill the .bss section. */
	ldr r0, =_bss
	ldr r1, =_bss_end
	movs r2, #0
	b bss_loop_start
bss_loop:
	str r2, [r0], #4
bss_loop_start:
	cmp r0, r1
	bcc bss_loop

	/* Copy the data section from flash to RAM. */
	ldr r0, =_data
	ldr r1, =_data_end
	ldr r2, =_idata
	b data_loop_start
data_loop:
	ldr r3, [r2], #4
	str r3, [r0], #4
data_loop_start:
	cmp r0, r1
	bcc data_loop

	/* All done, get started. */
	bl start

end: /* Infinite loop after start returns. */
	b end
.size _start, .-_start
