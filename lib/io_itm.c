/****************
 * ITM wrappers *
 ****************/

#include "io.h"
#include <stddef.h> /* NULL */

#define ITM_BASE (0xE0000000)
#define ITM_PORT(n) (*((volatile unsigned long *) (ITM_BASE + 4 * n)))
#define ITM_PORT_W(n) (*((volatile unsigned short *) (ITM_BASE + 4 * n)))
#define ITM_PORT_B(n) (*((volatile unsigned char *) (ITM_BASE + 4 * n)))
#define ITM_TER (*((volatile unsigned long *) (ITM_BASE + 0xE00)))
#define ITM_TCR (*((volatile unsigned long *) (ITM_BASE + 0xE80)))
#define ITM_TCR_ITMENA (1 << 0)

ioh_t *mitm = NULL;

int _mitmwrite(ioh_t *h, char *data, int len) {
	int dec = len;
	if (!(ITM_TCR & ITM_TCR_ITMENA) || !(ITM_TER & 1))
		return 0;
	while (dec >= 4) {
		while (ITM_PORT(0) == 0);
		ITM_PORT(0) = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
		data += 4;
		dec -= 4;
	}
	if (dec >= 2) {
		while (ITM_PORT(0) == 0);
		ITM_PORT_W(0) = data[0] | (data[1] << 8);
		data += 2;
		dec -= 2;
	}
	if (dec) {
		while (ITM_PORT(0) == 0);
		ITM_PORT_B(0) = *data++;
	}
	return len;
}

ioh_t *mitmopen(int options) {
	return _mcbopen(NULL, 0, options, NULL, &_mitmwrite, NULL, NULL, NULL);
}

void mitmprintf(char *fmt, ...) {
	if (mitm == NULL)
		mitm = mitmopen(0);
	va_list ap;
	va_start(ap, fmt);
	mvafprintf(mitm, fmt, ap);
	va_end(ap);
}
