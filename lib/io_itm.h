#ifndef __IO_ITM_H__
#define __IO_ITM_H__

#include "io.h"

extern ioh_t *mitm;

extern int _mitmwrite(ioh_t *h, char *data, int len);
extern ioh_t *mitmopen(int options);

extern void mitmprintf(char *fmt, ...);

#endif /* __IO_ITM_H__ */
