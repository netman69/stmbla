/* MAT stdlib.h for STM32.
 * 
 * Copyright (C) 2018 Mattis Michel.
 */
#ifndef __STDLIB_H__
#define __STDLIB_H__

#include <stddef.h> /* size_t */
#include <stdint.h> /* uint32_t */

extern void *malloc(size_t size);
extern void *realloc(void *ptr, size_t size);
extern void free(void *ptr);

#endif /* __STDLIB_H__ */
