/* MAT stddef.h for STM32.
 * 
 * Copyright (C) 2018 Mattis Michel.
 */
#ifndef __STDDEF_H__
#define __STDDEF_H__

#define NULL ((void *) 0)
typedef unsigned int size_t;

#endif /* __STDDEF_H__ */
