/* MAT io.c for STM32, printf implementation.
 * 
 * Copyright (C) 2018 Mattis Michel.
 */
#include <stddef.h> /* NULL */
#include <stdlib.h> /* malloc(), realloc(), free() */
#include <string.h> /* strlen(), memcpy() */
#include <stdarg.h>
#include "io.h"

const char hex[16] = {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
};

int mfread(ioh_t *h, char *data, int len) {
	if (h == NULL)
		return -1;
	if (h->read == NULL)
		return -1;
	return h->read(h, data, len);
}

int mfwrite(ioh_t *h, char *data, int len) {
	if (h == NULL)
		return -1;
	if (h->write == NULL)
		return -1;
	while (len--) {
		if (*data == '\n') {
			if (h->options & MFO_DOSNL)
				h->buf[h->pos++] = '\r';
			h->buf[h->pos++] = '\n';
			if (mfflush(h) != 0)
				return -1;
			++data;
			continue;
		}
		h->buf[h->pos++] = *(data++);
		if (h->pos >= sizeof(h->buf) - 2) /* leave 2 for newline always */
			if (mfflush(h) != 0)
				return -1;
	}
	if (h->options & MFO_DIRECT)
		mfflush(h);
	return 0;
}

int mfseek(ioh_t *h, int off, int whence) {
	if (h == NULL)
		return -1;
	if (h->seek == NULL)
		return -1;
	return h->seek(h, off, whence);
}

int mfflush(ioh_t *h) {
	if (h == NULL)
		return -1;
	if (h->write == NULL)
		return -1;
	if (h->write(h, h->buf, h->pos) < 0)
		return -1;
	h->pos = 0;
	return 0;
}

int mftrunc(ioh_t *h, int len) {
	if (h == NULL)
		return -1;
	if (h->trunc(h, len) < 0)
		return -1;
	h->pos = 0;
	return 0;
}

void mfclose(ioh_t *h) {
	if (h == NULL)
		return;
	mfflush(h);
	if (h->close != NULL)
		h->close(h);
	free(h->data);
	free((void *) h);
}

int mfprint(ioh_t *h, char *data) {
	return mfwrite(h, data, strlen(data));
}

int mfprintsnum(ioh_t *h, signed long long n, int b, int p, int plus) {
	if (n < 0) {
		mfwrite(h, "-", 1);
		n = -n;
	} else if (plus) mfwrite(h, "+", 1);
	return mfprintnum(h, n, b, p);
}

int mfprintnum(ioh_t *h, unsigned long long n, int b, int p) {
	char rev[64];
	int pos = sizeof(rev);
	do {
		rev[--pos] = hex[n % b];
		n /= b;
		--p;
	} while (n || p > 0);
	return mfwrite(h, rev + pos, sizeof(rev) - pos);
}

int mfprintf(ioh_t *h, char *fmt, ...) {
	int r;
	va_list ap;
	va_start(ap, fmt);
	r = mvafprintf(h, fmt, ap);
	va_end(ap);
	return r;
}

/* Must be a macro for the va_arg stuff to work everywhere (arm-none-eabi-gcc needed this). */
#define mprintf_getsnarg(l, ap) {\
	switch (l) {\
		case 1:\
			sn = va_arg(ap, signed long int);\
			break;\
		case 2:\
			sn = va_arg(ap, signed long long int);\
			break;\
		case 0:\
		default:\
			sn = va_arg(ap, signed int);\
	}\
}

/* Must be a macro for the va_arg stuff to work everywhere (arm-none-eabi-gcc needed this). */
#define mprintf_getnarg(l, _ap) {\
	switch (l) {\
		case 1:\
			n = va_arg(_ap, unsigned long int);\
			break;\
		case 2:\
			n = va_arg(_ap, unsigned long long int);\
			break;\
		case 0:\
		default:\
			n = va_arg(_ap, unsigned int);\
	}\
}

int mvafprintf(ioh_t *h, char *fmt, va_list ap) {
	char *start, *s;
	unsigned long long n = 0; /* They are just written 0 to suppress warnings. */
	signed long long sn = 0;
	unsigned char c;
	int p, l, plus;

	start = fmt;
	while (*fmt) {
		if (*fmt == '%') {
			if (mfwrite(h, start, fmt - start) < 0)
				return -1;
			++fmt;
			p = 0;
			while (*fmt >= '0' && *fmt <= '9') {
				p *= 10;
				p += *fmt - '0';
				++fmt;
			}
			l = 0;
			plus = 0;
			while (1) {
				switch (*fmt) {
					case 'l':
						++l;
						break;
					case 'q':
						l = 2;
						break;
					case '+':
						++plus;
						break;
					default:
						goto lsend;
				}
				++fmt;
			}
			lsend:
			switch (*fmt) {
				case 'c':
					mprintf_getnarg(l, ap);
					mfwrite(h, (char *) &c, 1);
					break;
				case 's':
					s = va_arg(ap, char *);
					if (s == NULL)
						s = "[NULL]";
					if (mfprint(h, s) < 0)
						return -1;
					break;
				case 'S':
					s = va_arg(ap, char *);
					n = va_arg(ap, int);
					if (s == NULL) {
						s = "[NULL]";
						if (mfprint(h, s) < 0)
							return -1;
					}
					if (mfwrite(h, s, n) < 0)
						return -1;
					break;
				case 'x':
				case 'X':
					mprintf_getnarg(l, ap);
					mfprintnum(h, n, 16, p);
					break;
				case 'i':
				case 'd':
					mprintf_getsnarg(l, ap);
					mfprintsnum(h, sn, 10, p, plus);
					break;
				case 'u':
					mprintf_getnarg(l, ap);
					mfprintnum(h, n, 10, p);
					break;
				case 'o':
					mprintf_getnarg(l, ap);
					mfprintnum(h, n, 8, p);
					break;
				case 'b':
					mprintf_getnarg(l, ap);
					mfprintnum(h, n, 2, p);
					break;
				default:
					if (mfwrite(h, fmt, 1) < 0)
						return -1;
					break;
			}
			start = ++fmt;
		} else ++fmt;
	}
	if (mfwrite(h, start, fmt - start) < 0)
		return -1;
	return 0;
}

/* generic constructor */
ioh_t *_mcbopen(void *d, int len, int options,
                int (*read)(ioh_t *, char *, int),
                int (*write)(ioh_t *, char *, int),
                int (*seek)(ioh_t *, int, int),
                int (*trunc)(ioh_t *, int),
                void (*close)(ioh_t *)) {
	ioh_t *new = (ioh_t *) malloc(sizeof(ioh_t));
	if (new == NULL)
		return NULL;
	new->data = malloc(len);
	if (new->data == NULL) {
		free(new);
		return NULL;
	}
	if (d != NULL)
		memcpy(new->data, d, len);
	new->read = read;
	new->write = write;
	new->seek = seek;
	new->trunc = trunc;
	new->close = close;
	new->pos = 0;
	new->options = options;
	return new;
}

/*******************
 * memory wrappers *
 *******************/

typedef struct {
	char *ptr;
	int pos, len, options;
} mmemdata_t;

int _mmemread(ioh_t *h, char *data, int len) {
	mmemdata_t *d = (mmemdata_t *) h->data;
	mfflush(h);
	if (d->ptr == NULL)
		return 0;
	if (len > d->len - d->pos)
		len = d->len - d->pos;
	memcpy((void *) ((char *) d->ptr + d->pos), data, len);
	return len;
}

int _mmemwrite(ioh_t *h, char *data, int len) {
	mmemdata_t *d = (mmemdata_t *) h->data;
	d->len += len;
	if (d->len < len)
		return -1;
	d->ptr = (char *) realloc((void *) d->ptr, d->len);
	if (d->ptr == NULL)
		return -1;
	memcpy((char *) d->ptr + d->pos, data, len);
	d->pos += len;
	return len;
}

int _mmemseek(ioh_t *h, int off, int whence) {
	mmemdata_t *d;
	d = (mmemdata_t *) h->data;
	mfflush(h);
	switch (whence) {
		case MSEEK_SET:
			d->pos = off;
			break;
		case MSEEK_CUR:
			d->pos += off;
			break;
		case MSEEK_END:
			d->pos = d->len - off;
			break;
	}
	return 0;
}

int _mmemtrunc(ioh_t *h, int len) {
	mmemdata_t *d = (mmemdata_t *) h->data;
	mfflush(h);
	d->len = len;
	if (d->pos > d->len)
		d->pos = d->len;
	d->ptr = (char *) realloc((void *) d->ptr, len);
	if (d->ptr == NULL)
		return -1;
	return 0;
}

void _mmemclose(ioh_t *h) {
	mmemdata_t *d = (mmemdata_t *) h->data;
	if (d->options & MMO_FREE)
		free((void *) ((mmemdata_t *) h->data)->ptr);
}

ioh_t *mmemopen(int options) {
	mmemdata_t d;
	d.ptr = NULL;
	d.pos = 0;
	d.len = 0;
	d.options = options;
	return _mcbopen((void *) &d, sizeof(mmemdata_t), 0, &_mmemread, &_mmemwrite, &_mmemseek, &_mmemtrunc, &_mmemclose);
}

char *mmemget(ioh_t *h) {
	if (h == NULL)
		return NULL;
	mfflush(h);
	return ((mmemdata_t *) h->data)->ptr;
}

int mmemlen(ioh_t *h) {
	if (h == NULL)
		return -1;
	mfflush(h);
	return ((mmemdata_t *) h->data)->len;
}

char *msprintf(char *fmt, ...) {
	ioh_t *h = mmemopen(0);
	char *ret;
	va_list ap;
	va_start(ap, fmt);
	mvafprintf(h, fmt, ap);
	va_end(ap);
	mfwrite(h, "\0", 1);
	ret = mmemget(h);
	mfclose(h);
	return ret;
}
