/* MAT string.c for STM32.
 * 
 * Copyright (C) 2018 Mattis Michel.
 */
#include <string.h>
#include <stdint.h> /* uint8_t */

size_t strlen(char *str) {
	int ret = 0;
	while (*(str++) != 0)
		++ret;
	return ret;
}

void *memset(void *dest, int c, size_t n) {
	while (n--)
		*((uint8_t *) dest++) = c;
	return dest;
}

void *memcpy(void *dest, const void *src, size_t n) {
	while (n--)
		*((uint8_t *) dest++) = *((uint8_t *) src++);
	return dest;
}
