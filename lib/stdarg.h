/* MAT stdarg.h for STM32.
 * 
 * Copyright (C) 2018 Mattis Michel.
 */
#ifndef __STDARG_H__
#define __STDARG_H__

typedef __builtin_va_list va_list;
#define va_start(v,l)  __builtin_va_start(v,l)
#define va_end(v)      __builtin_va_end(v)
#define va_arg(v,l)    __builtin_va_arg(v,l)
#define __va_copy(d,s) __builtin_va_copy(d,s)

#endif /* __STDARG_H__ */
