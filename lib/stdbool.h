/* MAT stdbool.h for STM32.
 * 
 * Copyright (C) 2018 Mattis Michel.
 */
#ifndef __STDBOOL_H__
#define __STDBOOL_H__

#include <stdint.h> /* uint8_t */

typedef uint8_t bool;

#define true  1
#define false 0

#endif /* __STDBOOL_H__ */
