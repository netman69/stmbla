#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
	static char buf[4096];
	int len, pos, rem = 0;
	int fd = open("itm.fifo", O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "can't open fifo\n");
		return 1;
	}
	rem  = 0;
	while ((len = read(fd, buf, sizeof(buf))) >= 0) {
		pos = 0;
		if (rem > 0) {
			int l = (rem < len) ? rem : len;
			fwrite(buf, 1, l, stdout);
			rem -= l;
			if (rem) continue;
			pos += l;
		}
		while (pos < len) {
			int l = 1 << buf[pos] >> 1;
			if (buf[pos] <= 0 || buf[pos] > 3) {
				++pos;
				continue;
			}
			if (len - pos - 1 < l) {
				rem = l - (len - pos - 1);
				l = len - pos - 1;
			}
			fwrite(buf + pos + 1, 1, l, stdout);
			pos += l + 1;
		}
		fflush(stdout);
	}
	close(fd);
	return 0;
}
