#ifndef __USB_H__
#define __USB_H__

#include <stdint.h>
#include <stddef.h> /* NULL, size_t */
#include <stdbool.h> /* bool, true, false */

#define MUSB_CTRL_BUF_SIZE  (128) /* Maximum size of data OUT stage for endpoint 0 (we need a buffer that size for it). */
#define MUSB_CTRL_DATA_MAX  (64)  /* Maximum size of data OUT packet for endpoint 0. */
#define MUSB_BTABLEADDR     (0x00) /* We can choose where to put the BTABLE, we put it at address 0 in the PMA. */
#define MUSB_PMAFREE_START  (0x20) /* First 64 bytes reserved for BTABLE (it's that size with the maximum of 8 entries). */

/**************************/
/* Peripheral definitions */
/**************************/

#define MUSB_BASE                    (0x40005C00)
#define MUSB_PMAADDR                 (0x40006000)

/* Common registers. */
#define MUSB_CNTR   (*(volatile uint16_t *) (MUSB_BASE + 0x40))
#define MUSB_ISTR   (*(volatile uint16_t *) (MUSB_BASE + 0x44))
#define MUSB_FNR    (*(volatile uint16_t *) (MUSB_BASE + 0x48))
#define MUSB_DADDR  (*(volatile uint16_t *) (MUSB_BASE + 0x4C))
#define MUSB_BTABLE (*(volatile uint16_t *) (MUSB_BASE + 0x50))

/* Endpoint specific registers. */
#define MUSB_EP(ep)                  (*(volatile uint16_t *) (MUSB_BASE + (ep) * 4))
#define MUSB_EP_MASK_AND             (MUSB_EP_SETUP | MUSB_EP_TYPE_MASK | MUSB_EP_KIND | MUSB_EP_ADDR_MASK)
#define MUSB_EP_MASK_OR              (MUSB_EP_CTR_RX | MUSB_EP_CTR_TX)
#define MUSB_EP_MASK(x)              (((x) & (MUSB_EP_MASK_AND)) | MUSB_EP_MASK_OR)
#define MUSB_EP_MASKED(ep)           (MUSB_EP_MASK(MUSB_EP(ep))) /* Clears and sets bits of the EP so nothing changes if written back as-is. */
#define MUSB_EP_RX_STAT_SET(ep, x)   (MUSB_EP(ep) = ((MUSB_EP(ep) & (MUSB_EP_MASK_AND | MUSB_EP_RX_STAT)) | MUSB_EP_MASK_OR) ^ (x))
#define MUSB_EP_TX_STAT_SET(ep, x)   (MUSB_EP(ep) = ((MUSB_EP(ep) & (MUSB_EP_MASK_AND | MUSB_EP_TX_STAT)) | MUSB_EP_MASK_OR) ^ (x))
#define MUSB_EP_RXTX_STAT_SET(ep, x) (MUSB_EP(ep) = ((MUSB_EP(ep) & (MUSB_EP_MASK_AND | MUSB_EP_RX_STAT | MUSB_EP_TX_STAT)) | MUSB_EP_MASK_OR) ^ (x))
#define MUSB_BT_ADDR_TX(ep)          (*(volatile uint16_t *) (MUSB_PMAADDR + MUSB_BTABLEADDR * 2 + ep * 16 +  0))
#define MUSB_BT_COUNT_TX(ep)         (*(volatile uint16_t *) (MUSB_PMAADDR + MUSB_BTABLEADDR * 2 + ep * 16 +  4))
#define MUSB_BT_ADDR_RX(ep)          (*(volatile uint16_t *) (MUSB_PMAADDR + MUSB_BTABLEADDR * 2 + ep * 16 +  8))
#define MUSB_BT_COUNT_RX(ep)         (*(volatile uint16_t *) (MUSB_PMAADDR + MUSB_BTABLEADDR * 2 + ep * 16 + 12)) /* Confusing register, see RM0008. */

/* Endpoint register. */
#define MUSB_EP_TX_STAT   (3 << 4)
#define MUSB_EP_DTOG_TX   (1 << 6)
#define MUSB_EP_CTR_TX    (1 << 7)
#define MUSB_EP_KIND      (1 << 8)
#define MUSB_EP_TYPE_MASK (3 << 9)
#define MUSB_EP_SETUP     (1 << 11)
#define MUSB_EP_RX_STAT   (3 << 12)
#define MUSB_EP_DTOG_RX   (1 << 14)
#define MUSB_EP_CTR_RX    (1 << 15)
#define MUSB_EP_ADDR_MASK (0x0F)

/* Endpoint type. */
#define MUSB_EP_BULK        (0x0000)
#define MUSB_EP_CONTROL     (0x0200)
#define MUSB_EP_ISOCHRONOUS (0x0400)
#define MUSB_EP_INTERRUPT   (0x0600)

/* TX and RX status. */
#define MUSB_EP_TX_DIS      (0x00)
#define MUSB_EP_TX_STALL    (0x10)
#define MUSB_EP_TX_NAK      (0x20)
#define MUSB_EP_TX_VALID    (0x30)
#define MUSB_EP_TX_DTOG1    (0x10)
#define MUSB_EP_TX_DTOG2    (0x20) // TODO this makes not a sense to me at the moment hmm
#define MUSB_EP_TX_DTOGMASK (MUSB_EP_TX_STAT | MUSB_EPREG_MASK) // TODO this does not make sense anymore
#define MUSB_EP_RX_DIS      (0x0000)
#define MUSB_EP_RX_STALL    (0x1000)
#define MUSB_EP_RX_NAK      (0x2000)
#define MUSB_EP_RX_VALID    (0x3000)
#define MUSB_EP_RX_DTOG1    (0x1000)
#define MUSB_EP_RX_DTOG2    (0x2000)
#define MUSB_EP_RX_DTOGMASK (MUSB_EP_RX_STAT | MUSB_EPREG_MASK)

/* CNTR register. */
#define MUSB_CNTR_FRES    (1 << 0)
#define MUSB_CNTR_PDWN    (1 << 1)
#define MUSB_CNTR_LP_MODE (1 << 2)
#define MUSB_CNTR_FSUSP   (1 << 3)
#define MUSB_CNTR_RESUME  (1 << 4)
#define MUSB_CNTR_ESOFM   (1 << 8)
#define MUSB_CNTR_SOFM    (1 << 9)
#define MUSB_CNTR_RESETM  (1 << 10)
#define MUSB_CNTR_SUSPM   (1 << 11)
#define MUSB_CNTR_WKUPM   (1 << 12)
#define MUSB_CNTR_ERRM    (1 << 13)
#define MUSB_CNTR_PMAOVRM (1 << 14)
#define MUSB_CNTR_CTRM    (1 << 15)

/* ISTR register. */
#define MUSB_ISTR_EP_ID  (0x0F)
#define MUSB_ISTR_DIR    (1 << 4)
#define MUSB_ISTR_ESOF   (1 << 8)
#define MUSB_ISTR_SOF    (1 << 9)
#define MUSB_ISTR_RESET  (1 << 10)
#define MUSB_ISTR_SUSP   (1 << 11)
#define MUSB_ISTR_WKUP   (1 << 12)
#define MUSB_ISTR_ERR    (1 << 13)
#define MUSB_ISTR_PMAOVR (1 << 14)
#define MUSB_ISTR_CTR    (1 << 15)

/* FNR register. */
#define MUSB_FNR_FN       (0x7FF)
#define MUSB_FNR_LSOF_OFF (11)
#define MUSB_FNR_LSOF     (3 << 11)
#define MUSB_FNR_LCK      (1 << 13)
#define MUSB_FNR_RXDM     (1 << 14)
#define MUSB_FNR_RXDP     (1 << 15)

/* DADDR register. */
#define MUSB_DADDR_ADD (0x7F)
#define MUSB_DADDR_EF  (1 << 7)

/* BTABLE COUNT_RX registers. */
#define MUSB_BTABLE_COUNT_RX_COUNT         (0x3FF)
#define MUSB_BTABLE_COUNT_RX_NUM_BLOCK_OFF (10)
#define MUSB_BTABLE_COUNT_RX_NUM_BLOCK     (0x1F << 10)
#define MUSB_BTABLE_COUNT_RX_BLSIZE        (1 << 15)

/********************************/
/* Protocol related definitions */
/********************************/

/* USB standard requests. */
#define MUSB_STDR_GET_STATUS        (0x00)
#define MUSB_STDR_CLEAR_FEATURE     (0x01)
#define MUSB_STDR_SET_FEATURE       (0x03)
#define MUSB_STDR_SET_ADDRESS       (0x05)
#define MUSB_STDR_GET_DESCRIPTOR    (0x06)
#define MUSB_STDR_SET_DESCRIPTOR    (0x07)
#define MUSB_STDR_GET_CONFIGURATION (0x08)
#define MUSB_STDR_SET_CONFIGURATION (0x09)
#define MUSB_STDR_GET_INTERFACE     (0x0A)
#define MUSB_STDR_SET_INTERFACE     (0x11)
#define MUSB_STDR_SYNCH_FRAME       (0x12)
#define MUSB_STDR_INVALID           (0xFF)

/* Masks and values for bmRequestType. */
#define MUSB_RT_RECIPIENT_MASK      (0x1F)
#define MUSB_RT_TYPE_MASK           (3 << 5)
#define MUSB_RT_DIR_MASK            (1 << 7)
#define MUSB_RT_RECIPIENT_DEVICE    (0x00)
#define MUSB_RT_RECIPIENT_INTERFACE (0x01)
#define MUSB_RT_RECIPIENT_ENDPOINT  (0x02)
#define MUSB_RT_RECIPIENT_OTHER     (0x03)
#define MUSB_RT_TYPE_STANDARD       (0x00 << 5)
#define MUSB_RT_TYPE_CLASS          (0x01 << 5)
#define MUSB_RT_TYPE_VENDOR         (0x02 << 5)
#define MUSB_RT_DIR                 (1 << 7)

/* Bits for device status (usb_device_status). */
#define MUSB_DEVICE_STATUS_SELF_POWERED  (1 << 0)
#define MUSB_DEVICE_STATUS_REMOTE_WAKEUP (1 << 1)

/* Values for SET_FEATURE and CLEAR_FEATURE. */
#define MUSB_FEATURE_ENDPOINT_HALT        (0)
#define MUSB_FEATURE_DEVICE_REMOTE_WAKEUP (1)
#define MUSB_FEATURE_TEST_MODE            (2)

typedef struct {
	uint8_t bmRequestType;
	uint8_t bRequest;
	uint16_t wValue;
	uint16_t wIndex;
	uint16_t wLength;
} __attribute__((packed)) usb_setup_t;

typedef struct {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint16_t bcdUSB;
	uint8_t bDeviceClass;
	uint8_t bDeviceSubClass;
	uint8_t bDeviceProtocol;
	uint8_t bMaxPacketSize; /* Only for EP 0, valid Sizes are 8, 16, 32, 64. */
	uint16_t idVendor;
	uint16_t idProduct;
	uint16_t bcdDevice;
	uint8_t iManufacturer;
	uint8_t iProduct;
	uint8_t iSerialNumber;
	uint8_t bNumConfigurations;
} __attribute__((packed)) usb_device_t;

typedef struct {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint16_t wTotalLength;
	uint8_t bNumInterfaces;
	uint8_t bConfigurationValue;
	uint8_t iConfiguration;
	uint8_t bmAttributes;
	uint8_t bMaxPower;
} __attribute__((packed)) usb_configuration_t;

typedef struct {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint8_t bInterfaceNumber;
	uint8_t bAlternateSetting;
	uint8_t bNumEndpoints;
	uint8_t bInterfaceClass;
	uint8_t bInterfaceSubClass;
	uint8_t bInterfaceProtocol;
	uint8_t iInterface;
} __attribute__((packed)) usb_interface_t;

typedef struct {
	uint8_t bLength;
	uint8_t bDescriptorType;
	uint8_t bEndpointAddress;
	uint8_t bmAttributes;
	uint16_t wMaxPacketSize;
	uint8_t bInterval;
} __attribute__((packed)) usb_endpoint_t;

/*************************************/
/* Exported variables and functions. */
/*************************************/

enum {
	MUSB_STATE_DEFAULT = 0,
	MUSB_STATE_ADDRESSING,
	MUSB_STATE_ADDRESSED,
	MUSB_STATE_CONFIGURED,
	MUSB_STATE_SUSPENDED_FLAG = 0x80
};

extern uint8_t usb_state;
extern uint16_t usb_device_status; /* Response for GET_STATUS standard device request. */
extern uint8_t usb_configuration; /* SET_CONFIGURATION and GET_CONFIGURATION set and read this. */

extern void usb_init(const usb_device_t *desc_dev, const usb_configuration_t *desc_conf, const char **desc_strings);
extern void usb_init_ep(uint8_t ep, uint16_t type, size_t rx_max, size_t tx_max);
extern void usb_int(void);
extern void usb_pma_write(uint16_t pma_addr, volatile uint8_t *buf, uint16_t len);
extern void usb_pma_read(uint16_t pma_addr, volatile uint8_t *buf, uint16_t len);
extern void usb_ctrl_write(void *buf, size_t len);
extern void usb_rwakeup(void);

/*********************************/
/* To be implemented by the user */
/*********************************/

/* Mandatory. */
extern void usbh_reset(void); /* Reset handler, for setting up endpoints etc (set_configuration/set_interface may want to set endpoint status though, when halted by setfeature those are supposed to un-halt it). */

/* Optional, default implementations provided as weak aliases. */
extern bool usbh_setup(usb_setup_t *s, void *data); /* Setup request, find info in usb_ctrl_status, either call usb_ctrl_ack/nak/write and return true, or return false if not handled. */
extern void usbh_set_configuration(uint8_t conf); /* Should set usb_configuration and call usb_ctrl_ack(), or call usb_ctrl_nak(). */
extern void usbh_get_interface(void); /* Same deal as usbh_get_descriptor, expects 1 byte to be written. */
extern void usbh_set_interface(uint8_t iface); /* Call usb_ctrl_ack/nak. */
extern void usbh_synch_frame(void); /* Synchronization frame for isochronous transfers and reading FNR. Write 2 bytes or call usb_ctrl_nak. */
extern void usbh_set_remote_wakeup(bool value); /* Called when SET_ or CLEAR_FEATURE change remote wakeup setting. */
extern void usbh_set_ep_halt(uint8_t ep, bool value); /* Called when endpoint halt feature set/cleared, bit 7 of ep indicates direction (1 == IN/TX) */
extern void usbh_set_address(uint8_t addr); /* Called when address has been set. */
extern void usbh_sof(void); /* Called when SOF occurs. */
extern void usbh_esof(void); /* Called when ESOF occurs. */
extern void usbh_ep_in(uint8_t ep);
extern void usbh_ep_out(uint8_t ep);
extern void usbh_suspend(void); /* Called on SUSP event, if the device disables the oscillator here it's suggested to place code for restarting the oscillator and clear FSUSP bit here too for lower latency but i think putting that in USBWakeUp interrupt would be faster. */
extern void usbh_resume(void); /* Called on successful WKUP event. */

#endif /* __USB_H__ */
