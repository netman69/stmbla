#include "usb.h"
#include <string.h> /* memcpy(), strlen() */

uint8_t usb_state = MUSB_STATE_DEFAULT; // TODO make static what possible
uint16_t usb_device_status = 0; // TODO allow user to choose initial value?
uint8_t usb_configuration = 0; // TODO maybe make this static after we figure out whether it should be shared or not
static uint8_t usb_rwkupcnt = 0;
static uint8_t usb_esofcnt = 0;
static uint16_t usb_address = 0;
static uint16_t usb_pmafree = MUSB_PMAFREE_START; /* Where to allocate next RX or TX buffer in PMA. */

/* Descriptors. */
static const usb_device_t *usb_desc_dev;
static const usb_configuration_t *usb_desc_conf;
static const char **usb_desc_strings;

/* Global state for control transfers. */
static usb_setup_t usb_ctrl_setup;
static uint8_t usb_ctrl_data[MUSB_CTRL_BUF_SIZE]; // TODO I don't like having this buffer.
static uint16_t usb_ctrl_data_pos = 0;
static uint16_t usb_ctrl_data_len = 0;

/* Initialize everything in our state machine to startup conditions. */
static void usb_state_init(void) {
	usb_state = MUSB_STATE_DEFAULT;
	usb_device_status = 0;
	usb_configuration = 0;
	usb_rwkupcnt = 0;
	usb_esofcnt = 0;
	usb_address = 0;
	usb_pmafree = MUSB_PMAFREE_START;
}

/* Initialize USB device. */
void usb_init(const usb_device_t *desc_dev, const usb_configuration_t *desc_conf, const char **desc_strings) {
	/* Store the descriptors. */
	usb_desc_dev = desc_dev; /* Note that these have to remain existent after the call. */
	usb_desc_conf = desc_conf;
	usb_desc_strings = desc_strings;
	/* Start USB peripheral. */
	usb_state_init();
	MUSB_CNTR = MUSB_CNTR_FRES; /* To make sure PDWN bit is cleared. */
	MUSB_CNTR = 0;
	while ((MUSB_ISTR & MUSB_ISTR_RESET) == 0);
	MUSB_ISTR = 0; /* Clear any pending interrupts. */
	MUSB_CNTR = MUSB_CNTR_CTRM | MUSB_CNTR_WKUPM | MUSB_CNTR_SUSPM | MUSB_CNTR_RESETM | MUSB_CNTR_SOFM | MUSB_CNTR_ESOFM; /* set interrupts mask */
}

/* Force the peripheral to reset. */
static void usb_forcereset(void) {
	/* Save endpoint registers. */
	uint16_t EP[8] = {
		MUSB_EP(0), MUSB_EP(1), MUSB_EP(2), MUSB_EP(3),
		MUSB_EP(4), MUSB_EP(5), MUSB_EP(6), MUSB_EP(7),
	};
	/* Request reset. */
	MUSB_CNTR |= MUSB_CNTR_RESETM;
	MUSB_CNTR |= MUSB_CNTR_FRES;
	MUSB_CNTR &= ~MUSB_CNTR_FRES;
	while ((MUSB_ISTR & MUSB_ISTR_RESET) == 0); /* Wait for reset. */
	MUSB_ISTR = ~MUSB_ISTR_RESET;
	/* Restore registers. */
	MUSB_EP(0) = EP[0];
	MUSB_EP(1) = EP[1];
	MUSB_EP(2) = EP[2];
	MUSB_EP(3) = EP[3];
	MUSB_EP(4) = EP[4];
	MUSB_EP(5) = EP[5];
	MUSB_EP(6) = EP[6];
	MUSB_EP(7) = EP[7];
}

/* NOTE Do not include direction bit with endpoint number.
 * Keep in mind PMA memory can get full with buffers and this function does not handle that.
 * To have the same memory for RX and TX (if they're expected to never happen at the same time) set tx_max to 0.
 */
void usb_init_ep(uint8_t ep, uint16_t type, size_t rx_max, size_t tx_max) { // TODO How to deal with double buffed EPs?
	volatile register uint16_t epreg =
		(MUSB_EP(ep) & ~(MUSB_EP_RX_STAT | MUSB_EP_TX_STAT | MUSB_EP_TYPE_MASK | MUSB_EP_KIND | MUSB_EP_CTR_RX | MUSB_EP_CTR_RX))
		| MUSB_EP_RX_NAK | MUSB_EP_TX_NAK | type; /* Leaving DTOG unmasked and then rewriting it should clear it. */
	MUSB_EP(ep) = epreg;
	MUSB_BT_ADDR_TX(ep) = usb_pmafree;
	MUSB_BT_ADDR_RX(ep) = usb_pmafree + tx_max;
	MUSB_BT_COUNT_RX(ep) = ((rx_max > 62) ? (MUSB_BTABLE_COUNT_RX_BLSIZE | ((rx_max - 1) >> 5 << 10)) : (rx_max >> 1 << 10));
	MUSB_BT_COUNT_TX(ep) = 0;
	usb_pmafree += tx_max + rx_max;
}

/* Write PMA memory. */
void usb_pma_write(uint16_t pma_addr, volatile uint8_t *buf, uint16_t len) {
	volatile uint16_t *pmaptr = (uint16_t *) (MUSB_PMAADDR + (pma_addr << 1));
	while (len >= 2) {
		*pmaptr = buf[0] | (buf[1] << 8);
		pmaptr += 2;
		buf += 2;
		len -= 2;
	}
	if (len)
		*pmaptr = buf[0];
}

/* Read PMA memory. */
void usb_pma_read(uint16_t pma_addr, volatile uint8_t *buf, uint16_t len) {
	volatile uint16_t *pmaptr = (uint16_t *) (MUSB_PMAADDR + (pma_addr << 1));
	while (len >= 2) {
		*(uint16_t *) buf = *pmaptr;
		pmaptr += 2;
		buf += 2;
		len -= 2;
	}
	if (len)
		*(uint8_t *) buf = *pmaptr;
}

/* Data IN stage (transmit data to host). */
static void usb_ctrl_data_in(bool force) {
	size_t rem = usb_ctrl_data_len - usb_ctrl_data_pos, len = ((rem < MUSB_CTRL_DATA_MAX) ? rem : MUSB_CTRL_DATA_MAX);

	/* Prepare for transmission. */
	usb_pma_write(MUSB_BT_ADDR_TX(0), usb_ctrl_data + usb_ctrl_data_pos, len);
	MUSB_BT_COUNT_TX(0) = len;
	usb_ctrl_data_pos += len;

	/* Decide what the status should be. */
	if (len == 0 && !force)
		MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_VALID | MUSB_EP_TX_STALL); /* IN data stage has ended, enable receiving for the OUT status stage. */
	else if (rem > MUSB_CTRL_DATA_MAX)
		MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_STALL | MUSB_EP_TX_VALID); /* Stall RX until last packet so changing direction results in error (see RM0008). */
	else MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_NAK | MUSB_EP_TX_VALID);
}

/* Send data stage in response to setup packet. */
void usb_ctrl_write(void *buf, size_t len) {
	memcpy(usb_ctrl_data, buf, (len > sizeof(usb_ctrl_data)) ? sizeof(usb_ctrl_data) : len); // TODO Most of the time the data is just a constant or global...
	usb_ctrl_data_len = len;
	usb_ctrl_data_pos = 0;
	usb_ctrl_data_in(true);
}

static void usb_ctrl_getdesc(void) {
	uint8_t i, n;
	switch (usb_ctrl_setup.wValue >> 8) {
		case 1:
			usb_ctrl_write((void *) usb_desc_dev, (usb_ctrl_setup.wLength < sizeof(usb_device_t)) ? usb_ctrl_setup.wLength : sizeof(usb_device_t));
			break;
		case 2:
			usb_ctrl_write((void *) usb_desc_conf, (usb_ctrl_setup.wLength < usb_desc_conf->wTotalLength) ? usb_ctrl_setup.wLength : usb_desc_conf->wTotalLength);
			break;
		case 3:
			n = usb_ctrl_setup.wValue & 0xFF;
			for (i = 0; i <= n; ++i) if (usb_desc_strings[i] == NULL) {
				MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_STALL | MUSB_EP_TX_STALL);
				return;
			}
			usb_ctrl_write((void *) usb_desc_strings[n], (usb_ctrl_setup.wLength < usb_desc_strings[n][0]) ? usb_ctrl_setup.wLength : usb_desc_strings[n][0]);
			break;
		default:
			MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_STALL | MUSB_EP_TX_STALL);
	}
}

/* Handle setup packets. */
static void usb_setup(void) {
	if (usbh_setup(&usb_ctrl_setup, usb_ctrl_data)) /* Callback for the library user to handle the packet. */
		return;
	if ((usb_ctrl_setup.bmRequestType & MUSB_RT_TYPE_MASK) != MUSB_RT_TYPE_STANDARD)
		goto usb_ctrl_setup_err;
	switch (usb_ctrl_setup.bmRequestType & MUSB_RT_RECIPIENT_MASK) {
		case MUSB_RT_RECIPIENT_DEVICE:
			switch (usb_ctrl_setup.bRequest) {
				case MUSB_STDR_GET_STATUS:
					if (!(usb_state == MUSB_STATE_ADDRESSED || usb_state == MUSB_STATE_CONFIGURED) || usb_ctrl_setup.bmRequestType != MUSB_RT_DIR_MASK || usb_ctrl_setup.wValue != 0 || usb_ctrl_setup.wIndex != 0 || usb_ctrl_setup.wLength != 2)
						goto usb_ctrl_setup_err;
					usb_ctrl_write((void *) &usb_device_status, 2);
					break;
				case MUSB_STDR_CLEAR_FEATURE:
					if (!(usb_state == MUSB_STATE_ADDRESSED || usb_state == MUSB_STATE_CONFIGURED) || usb_ctrl_setup.bmRequestType != 0 || usb_ctrl_setup.wIndex != 0 || usb_ctrl_setup.wLength != 0)
						goto usb_ctrl_setup_err;
					if (usb_ctrl_setup.wValue == MUSB_FEATURE_DEVICE_REMOTE_WAKEUP)
						usb_device_status &= ~MUSB_DEVICE_STATUS_REMOTE_WAKEUP;
					else goto usb_ctrl_setup_err; /* TEST_MODE can not be cleared according to the specification. */
					usb_ctrl_write(NULL, 0);
					usbh_set_remote_wakeup(false);
					break;
				case MUSB_STDR_SET_FEATURE:
					if (!(usb_state == MUSB_STATE_ADDRESSED || usb_state == MUSB_STATE_CONFIGURED) || usb_ctrl_setup.bmRequestType != 0 || usb_ctrl_setup.wIndex != 0 || usb_ctrl_setup.wLength != 0)
						goto usb_ctrl_setup_err;
					if (usb_ctrl_setup.wValue == MUSB_FEATURE_DEVICE_REMOTE_WAKEUP)
						usb_device_status |= MUSB_DEVICE_STATUS_REMOTE_WAKEUP;
					else goto usb_ctrl_setup_err; /* TEST_MODE is a valid feature which is not implemented here, it is valid in default state. */
					usb_ctrl_write(NULL, 0);
					usbh_set_remote_wakeup(true);
					break;
				case MUSB_STDR_SET_ADDRESS:
					if (!(usb_state == MUSB_STATE_DEFAULT || usb_state == MUSB_STATE_ADDRESSED) || usb_ctrl_setup.bmRequestType != 0 || usb_ctrl_setup.wValue > 127 || usb_ctrl_setup.wIndex != 0 || usb_ctrl_setup.wLength != 0)
						goto usb_ctrl_setup_err;
					usb_address = usb_ctrl_setup.wValue;
					usb_state = MUSB_STATE_ADDRESSING;
					usb_ctrl_write(NULL, 0); /* Just acknowledge, set address in status stage. */
					break;
				case MUSB_STDR_GET_DESCRIPTOR:
					if (usb_ctrl_setup.bmRequestType != MUSB_RT_DIR_MASK) /* This one is valid in all states except when suspended. */
						goto usb_ctrl_setup_err;
					usb_ctrl_getdesc();
					break;
				case MUSB_STDR_GET_CONFIGURATION:
					if (!(usb_state == MUSB_STATE_ADDRESSED || usb_state == MUSB_STATE_CONFIGURED) || usb_ctrl_setup.bmRequestType != MUSB_RT_DIR_MASK || usb_ctrl_setup.wValue != 0 || usb_ctrl_setup.wIndex != 0 || usb_ctrl_setup.wLength != 1)
						goto usb_ctrl_setup_err;
					usb_ctrl_write((void *) &usb_configuration, 1);
					break;
				case MUSB_STDR_SET_CONFIGURATION:
					if (usb_state == MUSB_STATE_DEFAULT || usb_ctrl_setup.bmRequestType != 0 || usb_ctrl_setup.wIndex != 0 || usb_ctrl_setup.wLength != 0)
						goto usb_ctrl_setup_err;
					usbh_set_configuration(usb_ctrl_setup.wValue);
					usb_state = ((usb_configuration != 0) ? MUSB_STATE_CONFIGURED : MUSB_STATE_ADDRESSED);
					break;
				default:
					/* Note that SET_DESCRIPTOR also exists, is an optional standard request, which is currently not implemented. */
					goto usb_ctrl_setup_err;
			}
			break;
		case MUSB_RT_RECIPIENT_INTERFACE:
			if (usb_state != MUSB_STATE_CONFIGURED)
				goto usb_ctrl_setup_err;
			switch (usb_ctrl_setup.bRequest) {
				case MUSB_STDR_GET_STATUS:
					if (usb_ctrl_setup.bmRequestType != (MUSB_RT_DIR_MASK | MUSB_RT_RECIPIENT_INTERFACE) || usb_ctrl_setup.wValue != 0 || usb_ctrl_setup.wLength != 2)
						goto usb_ctrl_setup_err;
					uint16_t st = 0; /* Status is always 0 for interfaces. */
					usb_ctrl_write((void *) &st, 2);
					break;
				case MUSB_STDR_GET_INTERFACE:
					if (usb_ctrl_setup.bmRequestType != (MUSB_RT_DIR_MASK | MUSB_RT_RECIPIENT_INTERFACE) || usb_ctrl_setup.wValue != 0 || usb_ctrl_setup.wLength != 1)
						goto usb_ctrl_setup_err;
					usbh_get_interface();
					break;
				case MUSB_STDR_SET_INTERFACE:
					if (usb_ctrl_setup.bmRequestType != MUSB_RT_RECIPIENT_INTERFACE || usb_ctrl_setup.wLength != 0)
						goto usb_ctrl_setup_err;
					usbh_set_interface(usb_ctrl_setup.wValue);
					break;
				default:
					/* Note that CLEAR_FEATURE and SET_FEATURE are also standard interface requests, but no features exist in the standard. */
					goto usb_ctrl_setup_err;
			}
			break;
		case MUSB_RT_RECIPIENT_ENDPOINT:
			{
				uint16_t ep = usb_ctrl_setup.wIndex & ~0x80;
				uint8_t dir = usb_ctrl_setup.wIndex & 0x80;
				if ((usb_state != MUSB_STATE_CONFIGURED && !(usb_state == MUSB_STATE_ADDRESSED && ep == 0)) || ep > 7 || usb_ctrl_setup.wValue != 0)
					goto usb_ctrl_setup_err; /* SET_ and GET_FEATURE use wValue, but only 0 is valid. */
				switch (usb_ctrl_setup.bRequest) {
					case MUSB_STDR_GET_STATUS:
						if (usb_ctrl_setup.bmRequestType != (MUSB_RT_DIR_MASK | MUSB_RT_RECIPIENT_ENDPOINT) || usb_ctrl_setup.wLength != 2)
							goto usb_ctrl_setup_err;
						uint16_t st;
						if (dir) {
							st = (((MUSB_EP(ep) & MUSB_EP_TX_STAT) == MUSB_EP_TX_STALL) ? 1 : 0);
						} else {
							st = (((MUSB_EP(ep) & MUSB_EP_RX_STAT) == MUSB_EP_RX_STALL) ? 1 : 0);
						}
						usb_ctrl_write(&st, 2);
						break;
					case MUSB_STDR_CLEAR_FEATURE:
						if (usb_ctrl_setup.bmRequestType != (MUSB_RT_DIR_MASK | MUSB_RT_RECIPIENT_ENDPOINT) || usb_ctrl_setup.wLength != 0)
							goto usb_ctrl_setup_err;
						if (dir) {
							MUSB_EP_TX_STAT_SET(ep, MUSB_EP_TX_VALID); // TODO Am I sure about this?
							if (MUSB_EP(ep) & MUSB_EP_DTOG_TX) /* Clear DTOG. */
								MUSB_EP(ep) = MUSB_EP_MASKED(ep) | MUSB_EP_DTOG_TX;
						} else {
							MUSB_EP_RX_STAT_SET(ep, MUSB_EP_RX_VALID);
							if (MUSB_EP(ep) & MUSB_EP_DTOG_RX) /* Clear DTOG. */
								MUSB_EP(ep) = MUSB_EP_MASKED(ep) | MUSB_EP_DTOG_RX;
						}
						usbh_set_ep_halt(ep | dir, false);
						break;
					case MUSB_STDR_SET_FEATURE:
						if (ep == 0 || usb_ctrl_setup.bmRequestType != (MUSB_RT_DIR_MASK | MUSB_RT_RECIPIENT_ENDPOINT) || usb_ctrl_setup.wLength != 0)
							goto usb_ctrl_setup_err;
						if (dir)
							MUSB_EP_TX_STAT_SET(ep, MUSB_EP_TX_STALL);
						else MUSB_EP_RX_STAT_SET(ep, MUSB_EP_RX_STALL);
						usbh_set_ep_halt(ep | dir, true);
						break;
					case MUSB_STDR_SYNCH_FRAME:
						if (usb_state != MUSB_STATE_CONFIGURED || usb_ctrl_setup.bmRequestType != (MUSB_RT_DIR_MASK | MUSB_RT_RECIPIENT_ENDPOINT) || usb_ctrl_setup.wLength != 2)
							goto usb_ctrl_setup_err;
						usbh_synch_frame();
						break;
					default:
						goto usb_ctrl_setup_err;
				}
			}
			break;
		default:
			goto usb_ctrl_setup_err;
	}
	return;
usb_ctrl_setup_err:
	MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_STALL | MUSB_EP_TX_STALL);
}

/* Data OUT stage (receive data from host). */
static void usb_ctrl_data_out(void) {
	size_t len = MUSB_BT_COUNT_RX(0) & 0x3FF, maxlen = usb_ctrl_data_len - usb_ctrl_data_pos;
	len = (len < maxlen) ? len : maxlen;

	/* Get the received data (if any). */
	usb_pma_read(MUSB_BT_ADDR_RX(0), usb_ctrl_data + usb_ctrl_data_pos, len);
	usb_ctrl_data_pos += len;

	/* Quit when we have the requested amount of bytes. */
	if (usb_ctrl_data_len - usb_ctrl_data_pos == 0) {
		/* No more receiving, transfer complete handler has to decide whether to stall or give IN status packet. */
		MUSB_EP_RX_STAT_SET(0, MUSB_EP_RX_STALL);
		if ((usb_ctrl_setup.bmRequestType & MUSB_RT_DIR) == 0 && usb_ctrl_setup.wLength != 0)
			usb_setup();
		else MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_VALID | MUSB_EP_TX_STALL);
		return;
	}

	/* Opposite of what we do in TX stage. */
	if (maxlen > MUSB_CTRL_DATA_MAX)
		MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_VALID | MUSB_EP_TX_STALL);
	else MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_VALID | MUSB_EP_TX_NAK);
}

/* Handler for USB_LP_CAN1_RX0 and USB_HP_CAN1_TX interrupts. */
void usb_int(void) {
	uint16_t mIstr = MUSB_ISTR & (MUSB_CNTR | 0xFF);

	if (mIstr & MUSB_ISTR_CTR) { /* Note that CTR is a read-only bit, we don't need to clear it manually. */
		uint8_t ep = mIstr & MUSB_ISTR_EP_ID; /* Note that endopoint ID and endpoint number aren't necessarily the same but we set them the same. */
		uint16_t epreg = MUSB_EP(ep);

		if (mIstr & MUSB_ISTR_DIR) {
			/* OUT interrupt, it means we've received data.  */
			if (ep == 0) {
				if (epreg & MUSB_EP_SETUP && (MUSB_BT_COUNT_RX(0) & 0x3FF) == sizeof(usb_setup_t)) { /* Clearing CTR_RX also clears SETUP which is read-only. */
					/* SETUP stage, usb_setup() must ensure to set status to TX_VALID if ok or TX_STALL on error. */
					usb_pma_read(MUSB_BT_ADDR_RX(0), (void *) &usb_ctrl_setup, sizeof(usb_setup_t));
					if ((usb_ctrl_setup.bmRequestType & MUSB_RT_DIR) == 0 && usb_ctrl_setup.wLength != 0) {
						/* This message comes with data, initiate a read. */
						if (usb_ctrl_setup.wLength <= sizeof(usb_ctrl_data)) {
							usb_ctrl_data_len = usb_ctrl_setup.wLength;
							usb_ctrl_data_pos = 0;
							if (usb_ctrl_data_len > MUSB_CTRL_DATA_MAX)
								MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_VALID | MUSB_EP_TX_STALL); /* If there's multiple packets to be received stall the TX direction for now. */
							else MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_VALID | MUSB_EP_TX_NAK);
						} else MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_STALL | MUSB_EP_TX_STALL); /* Bail out if too much data wanted to send. */
					} else usb_setup(); // TODO Ensure TX_VALID or TX_STALL set in this.
				} else usb_ctrl_data_out();
			} else usbh_ep_out(ep);
			MUSB_EP(ep) = MUSB_EP_MASKED(ep) & ~MUSB_EP_CTR_RX; /* Note that ISTR will change after clearing this. */
		} else {
			/* IN interrupt, it means a transmit completed.  */
			if (ep == 0 && usb_state == MUSB_STATE_ADDRESSING) {
				/* If an address is given to the device, we have to give it the address after completing transmission of the ACK. */
				MUSB_DADDR = usb_address | MUSB_DADDR_EF;
				usb_state = MUSB_STATE_ADDRESSED;
				usbh_set_address(usb_address);
			}
			if (ep == 0) /* Continue transmitting data if necessary. */
				usb_ctrl_data_in(false);
			else usbh_ep_in(ep);
			MUSB_EP(ep) = MUSB_EP_MASKED(ep) & ~MUSB_EP_CTR_TX;
		}
	}

	if (mIstr & MUSB_ISTR_RESET) {
		int i;
		MUSB_BTABLE = MUSB_BTABLEADDR;
		/* Initialize endpoints. */
		for (i = 0; i < 8; ++i) /* USB-FS driver from ST only sets used endpoints and does this on address set for some reason. */
			MUSB_EP(i) = MUSB_EP_RX_DIS | MUSB_EP_TX_DIS | i;
		/* Set our address to 0. */
		MUSB_DADDR = 0 | MUSB_DADDR_EF;
		/* Reset our things. */
		usb_state_init();
		/* Initialize endpoint 0. */
		usb_init_ep(0, MUSB_EP_CONTROL, MUSB_CTRL_DATA_MAX, 0); /* We set tx_max to 0 so it'll be shared with RX area. */
		MUSB_EP_RX_STAT_SET(0, MUSB_EP_RX_VALID);
		/* Call user reset handler. */
		usbh_reset();
		MUSB_ISTR = ~MUSB_ISTR_RESET;
	}

	if (mIstr & MUSB_ISTR_ESOF) {
		if (usb_rwkupcnt > 0 && --usb_rwkupcnt == 0)
			MUSB_CNTR &= ~MUSB_CNTR_RESUME;
		if (!(MUSB_CNTR & MUSB_CNTR_FSUSP) && (MUSB_FNR & MUSB_FNR_RXDP) && usb_esofcnt++ > 4) {
			usb_forcereset(); /* Being here means it's likely we missed a SUSP interrupt, resetting makes it come again. */
			usb_esofcnt = 0;
		}
		usbh_esof();
		MUSB_ISTR = ~MUSB_ISTR_ESOF;
	}

	if (mIstr & MUSB_ISTR_SOF) {
		usbh_sof();
		usb_esofcnt = 0;
		MUSB_ISTR = ~MUSB_ISTR_SOF;
	}

	if (mIstr & MUSB_ISTR_SUSP) {
		usb_state |= MUSB_STATE_SUSPENDED_FLAG;
		usb_forcereset(); /* Needed to prevent some situations where wakeup event won't work. */
		MUSB_CNTR |= MUSB_CNTR_FSUSP;
		MUSB_CNTR |= MUSB_CNTR_LP_MODE; /* RM0008 is vague about whether we can combine this with FSUSP. */
		usbh_suspend();
		MUSB_ISTR = ~MUSB_ISTR_SUSP; /* This must be done after suspending in case we enter sleep mode and want to do stuff before WKUP interrupt handler or more SUSP events come in before ISR is done, i think. */
	}

	if (mIstr & MUSB_ISTR_WKUP) {
		MUSB_CNTR &= ~(MUSB_CNTR_FSUSP | MUSB_CNTR_LP_MODE);
		usb_state &= ~MUSB_STATE_SUSPENDED_FLAG;
		usbh_resume();
		MUSB_ISTR = ~MUSB_ISTR_WKUP;
	}
}

/* Initiate remote wakeup, note that this should be used only once the system clock is running normally. */
void usb_rwakeup(void) {
	if (usb_rwkupcnt || !(usb_state & MUSB_STATE_SUSPENDED_FLAG))
		return;
	MUSB_CNTR |= MUSB_CNTR_RESUME;
	usb_rwkupcnt = 2; /* This make sure it is held for longer than 1ms. */
}

/*************************************************/
/* Weak aliases for user implementable functions */
/*************************************************/

__attribute__((weak)) bool usbh_setup(usb_setup_t *s, void *data) {
	return false;
}

__attribute__((weak)) void usbh_set_configuration(uint8_t conf) {
	usb_configuration = conf;
	usb_ctrl_write(NULL, 0);
}

__attribute__((weak)) void usbh_get_interface(void) {
	uint8_t n = 0;
	usb_ctrl_write(&n, 1);
}

__attribute__((weak)) void usbh_set_interface(uint8_t iface) {
	if (iface == 0)
		usb_ctrl_write(NULL, 0);
	MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_STALL | MUSB_EP_TX_STALL);
}

__attribute__((weak)) void usbh_synch_frame(void) {
	MUSB_EP_RXTX_STAT_SET(0, MUSB_EP_RX_STALL | MUSB_EP_TX_STALL);
}

void usbh_empty(void) { /* Empty. */ }

__attribute__((weak, alias("usbh_empty"))) void usbh_sof(void);
__attribute__((weak, alias("usbh_empty"))) void usbh_esof(void);
__attribute__((weak, alias("usbh_empty"))) void usbh_suspend(void);
__attribute__((weak, alias("usbh_empty"))) void usbh_resume(void);

__attribute__((weak)) void usbh_set_remote_wakeup(bool value) { /* Empty. */ }
__attribute__((weak)) void usbh_set_ep_halt(uint8_t ep, bool value) { /* Empty. */ }
__attribute__((weak)) void usbh_set_address(uint8_t addr) { /* Empty. */ } // TODO figure whether this and other handlers are really sensible
__attribute__((weak)) void usbh_ep_in(uint8_t ep) { /* Empty. */ }
__attribute__((weak)) void usbh_ep_out(uint8_t ep) { /* Empty. */ }
